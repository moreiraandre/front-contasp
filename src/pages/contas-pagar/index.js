import React, {Component} from "react";
import api from '../../services/api';
import {Table, Form, Button, ButtonGroup, Container, Col, Pagination} from 'react-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js'

const endpoint = '/api/contas-pagar';

export default class ContasPagar extends Component {
    state = {
        dados: [],
        paginacao: null,
        mostrarForm: false,
        idAtual: 0,
        fornecedores: []
    }

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const response = await api.get('/api/fornecedor?page=N');
        const {data, ...paginacao} = response.data;
        this.setState({fornecedores: response.data.data});

        this.loadData();
    }

    loadData = async (url) => {
        url = url ? url : endpoint;
        const response = await api.get(url);
        const {data, ...paginacao} = response.data;
        this.setState({dados: response.data.data, paginacao: paginacao});
    };

    handleSubmit = async (event) => {
        const btnSubmit = document.querySelector('[type=submit]');
        btnSubmit.innerHTML = 'Aguarde...';
        event.preventDefault();
        let data = new FormData(event.target);

        try {
            if (this.state.idAtual > 0) {
                data.append('_method', 'put');
                await api.post(endpoint + '/' + this.state.idAtual, data);
            } else {
                await api.post(endpoint, data);
            }
            await this.loadData();
            document.getElementById('form-cadastro').reset(); // LIMPAR OS CAMPOS DO FORM
            this.setState({mostrarForm: false, idAtual: 0});

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                text: 'Sucesso!',
                showConfirmButton: false,
                timer: 1500
            });
        } catch (error) {
            Swal.fire({
                icon: 'error',
                text: 'Falha!'
            });
        }
        btnSubmit.innerHTML = 'Salvar';
    }

    clickPaginacao = async (event) => {
        event.preventDefault();
        try {
            await this.loadData(event.target.href);
        } catch (error) {
            Swal.fire({
                icon: 'error',
                text: 'Falha!'
            });
        }
    }

    handleExclusao = async (id) => {
        try {
            await api.delete(endpoint + '/' + id);
            await this.loadData();

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                text: 'Sucesso!',
                showConfirmButton: false,
                timer: 1500
            });
        } catch (error) {
            Swal.fire({
                icon: 'error',
                text: 'Falha!'
            });
        }
    }

    clickExclusao = async (event) => {
        event.preventDefault();
        const eventId = event.target.id;

        Swal.fire({
            text: "Tem certeza?",
            icon: 'question',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                this.handleExclusao(eventId);
            }
        });
    }

    clickEditar = async (event) => {
        event.preventDefault();
        const eventId = event.target.id;
        const response = await api.get(endpoint + '/' + eventId);
        const {data} = response.data;
        this.setState({mostrarForm: true, idAtual: eventId});
        let form = document.getElementById('form-cadastro');

        for (let field in data) {
            let htmlField = form.querySelector(`[name="${field}"]`);
            if (htmlField) {
                htmlField.value = data[field];
            }
        }
    }

    clickMostrarForm = async (event) => {
        this.setState({mostrarForm: true});
    }

    render() {
        let paginacao = '';
        if (this.state.paginacao) {
            paginacao = <Pagination size="sm">
                <Pagination.First onClick={this.clickPaginacao} href={this.state.paginacao.links.first}/>
                <Pagination.Prev onClick={this.clickPaginacao} href={this.state.paginacao.links.prev}/>
                <Pagination.Item onClick={this.clickPaginacao}
                                 active>{this.state.paginacao.meta.current_page}</Pagination.Item>
                <Pagination.Next onClick={this.clickPaginacao} href={this.state.paginacao.links.next}/>
                <Pagination.Last onClick={this.clickPaginacao} href={this.state.paginacao.links.last}/>
            </Pagination>;
        }

        return (
            <Container fluid>
                <Button variant="primary" type="button" style={{display: !this.state.mostrarForm ? 'block' : 'none'}}
                        onClick={this.clickMostrarForm}>
                    Inserir
                </Button>

                <Form id="form-cadastro" onSubmit={this.handleSubmit}
                      style={{display: this.state.mostrarForm ? 'block' : 'none'}}>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Label>Fornecedor</Form.Label>
                        <Form.Control as="select" name="fornecedor_id" defaultValue="Selecione...">
                            <option>Selecione...</option>
                            {this.state.fornecedores.map(fornecedor => (
                                <option value={fornecedor.id} key={fornecedor.id}>{fornecedor.cnpj} - {fornecedor.nome}</option>
                            ))}
                        </Form.Control>
                    </Form.Group>

                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridAddress1">
                            <Form.Label>Descrição</Form.Label>
                            <Form.Control name="descricao"/>
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridAddress1">
                            <Form.Label>Valor</Form.Label>
                            <Form.Control name="valor"/>
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridAddress1">
                            <Form.Label>Data vencimento</Form.Label>
                            <Form.Control type="date" name="data_vencimento"/>
                        </Form.Group>
                    </Form.Row>

                    <Button variant="primary" type="submit">
                        Salvar
                    </Button>
                </Form>
                <hr/>

                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Dt. venc.</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                        <th>Fornecedor</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.dados.map(row => (
                        <tr key={row.id}>
                            <td>{row.id}</td>
                            <td>{row.data_vencimento}</td>
                            <td>{row.descricao}</td>
                            <td>{row.valor}</td>
                            <td>{row.fornecedor}</td>
                            <td>
                                <ButtonGroup size="sm">
                                    <Button onClick={this.clickEditar} id={row.id}>Editar</Button>
                                    <Button onClick={this.clickExclusao} variant="danger" id={row.id}>Excluir</Button>
                                </ButtonGroup>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>

                {paginacao}
            </Container>
        );
    }
}
