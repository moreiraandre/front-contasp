import React from "react";

import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Main from "./pages/main";
import ContasPagar from "./pages/contas-pagar";
import Fornecedores from "./pages/fornecedores";

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={Main}/>
            <Route path='/fornecedores' component={Fornecedores}/>
            <Route path='/contas-pagar' component={ContasPagar}/>
        </Switch>
    </BrowserRouter>
)

export default Routes;
