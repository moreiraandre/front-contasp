import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Navbar extends Component {
    render() {
        return (
            <nav>
                <a href="/">Home</a> | <a href="/fornecedores">Fornecedores</a> | <a href="/contas-pagar">Contas a Pagar</a>
            </nav>
        )
    }
}
