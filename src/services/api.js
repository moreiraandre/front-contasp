import axios from 'axios';

const api = axios.create({
    baseURL: 'http://api.contasp.test',
    headers: {'Authorization': 'Bearer CODETESIS'}
});
export default api;
