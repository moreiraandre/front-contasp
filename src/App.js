import React from 'react';
import Routes from './routes'
import Navbar from "./components/Navbar";

import logo from './logo.svg';

const App = () => (
    <div className="App">
        <Navbar/>
        <Routes/>
    </div>
);

export default App;
